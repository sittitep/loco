class Api::V1::QuestionsController < Api::V1::BaseController
	before_action :set_question, only: [:show, :update, :vote_up, :vote_down]

	def show
		if @question
			render json: @question, status: 200
		else
			render json: {}, status: 400 
		end
	end

	def create
		question = current_user.questions.new params_question
		if question.save
			render json: question, status: 200
		else
			render json: question.errors.as_json, status: 400
		end
	end

	def update
		if @question.update_attributes params_question
			render json: @question, status: 200
		else
			render json: @question.errors.as_json, status: 400
		end	
	end

	def vote_up
		if @question.upvote_from current_user
			render json: @question, status: 200
		else
			render json: @question.errors.as_json, status: 400
		end	
	end

	def vote_down
		if @question.downvote_from current_user
			render json: @question, status: 200
		else
			render json: @question.errors.as_json, status: 400
		end	
	end

	private
		def set_question
			@question = Question.find_by_id params[:id]
			render json: {}, status: 400 unless @question
		end

		def params_question
			params.require(:question).permit(:header, :body)
		end
end
