class Answer < Message
	belongs_to :question, foreign_key: "id", class_name: "Question"

	acts_as_votable
end
