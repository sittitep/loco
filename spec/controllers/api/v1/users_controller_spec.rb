require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
	describe "POST create" do
    it "has a 200 status code" do
      post :create, user: attributes_for(:user)
      expect(response.status).to eq(200)
    end
  end

  describe "PUT update" do
  	it "has a 200 status code" do
  		user = create(:user)
  		put :update, {id: user.id, user: attributes_for(:user), token: user.token}
  		expect(response.status).to eq(200)
  	end
  end

  describe "GET authenticate" do
  	it "has a 200 status code" do
  		user = create(:user)
  		get :authenticate, {email:user.email, password: user.password}
  		expect(response.status).to eq(200)
  	end
  end
end
