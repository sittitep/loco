class Api::V1::BaseController < ApplicationController
	before_filter :authenticate_token

	def authenticate_token
		@current_user = User.where(token: params[:token]).first
		render nothing: true, status: 400 and return unless @current_user
	end
end
