FactoryGirl.define do
  sequence(:email) {|n| "person-#{n}@example.com" }
  sequence(:id) {|n| "{n}" }

  factory :user do
    email
    password  "password"
    token "1234"
  end

  factory :question do
  	id
    header "example question"
  	body "example content"

    user
  end

  factory :answer do
    id
  	body "example answer"
    question
  end
end