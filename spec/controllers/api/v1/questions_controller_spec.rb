require 'rails_helper'

RSpec.describe Api::V1::QuestionsController, type: :controller do
  describe "GET show" do
    it "has a 200 status code" do
      user = create(:user)
      question = user.questions.create attributes_for(:question)
      get :show, {id: user.questions.first.id, token: user.token}
      expect(response.status).to eq(200)
    end
  end

	describe "POST create" do
    it "has a 200 status code" do
      user = create(:user)
      post :create, {question: attributes_for(:question), token: user.token}
      expect(response.status).to eq(200)
    end
  end

  describe "PUT update" do
    it "has a 200 status code" do
      user = create(:user)
      question = user.questions.create attributes_for(:question)
      put :update, {id: user.questions.first.id, question: question.as_json, token: user.token}
      expect(response.status).to eq(200)
    end
  end

  describe "POST vote_up" do
    it "has a 200 status code" do
      user = create(:user)
      question = user.questions.create attributes_for(:question)
      post :vote_up, {id: user.questions.first.id, question: question.as_json, token: user.token}
      expect(response.status).to eq(200)
    end
  end

  describe "POST vote_down" do
    it "has a 200 status code" do
      user = create(:user)
      question = user.questions.create attributes_for(:question)
      post :vote_down, {id: user.questions.first.id, question: question.as_json, token: user.token}
      expect(response.status).to eq(200)
    end
  end
end
