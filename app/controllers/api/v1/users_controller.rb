class Api::V1::UsersController < Api::V1::BaseController
	skip_before_action :authenticate_api, only: [:create, :authenticate]

	def create
		user = User.new params_user
		if user.save
			render json: user, status: 200
		else
			render json: user.errors.as_json, status: 400
		end
	end

	def update
		if current_user.update_attributes(params_user)
			render json: @user, status: 200
		else
			render json: @user.errors.as_json, status: 400
		end
	end

	def authenticate
		user = User.where(email: params[:email]).first
		if user.try(:authenticate, params[:password])
			user.ensure_token!
			render json: user, status: 200
		else
			render json: {}, status: 400
		end
	end

	private
		def params_user
			params.require(:user).permit(:email, :password)
		end
end
