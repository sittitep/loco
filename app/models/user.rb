require 'securerandom'

class User < ActiveRecord::Base
	validates :email, presence: true
	validates :password, presence: true
	has_secure_password

	has_many :messages
	has_many :questions
	has_many :answer

	def ensure_token!
		api_token = nil
		until User.where(token: api_token).first.blank? && api_token.present? do
			api_token = SecureRandom.hex
		end
		self.token = api_token
		self.save(validate: false)
	end
end
