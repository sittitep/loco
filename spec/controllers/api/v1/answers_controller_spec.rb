require 'rails_helper'

RSpec.describe Api::V1::AnswersController, type: :controller do
  describe "GET show" do
    it "has a 200 status code" do
      question = create(:question)
      get :show, {id: question.id, user_id: question.user.id}
      expect(response.status).to eq(200)
    end
  end

	describe "POST create" do
    it "has a 200 status code" do
      question = create(:question)
      post :create, {user_id: question.user.id, question: question.as_json}
      expect(response.status).to eq(200)
    end
  end

  describe "PUT update" do
    it "has a 200 status code" do
      question = create(:question)
      put :update, {user_id: question.user.id, id: question.id, question: question.as_json}
      expect(response.status).to eq(200)
    end
  end

  describe "POST vote_up" do
    it "has a 200 status code" do
      question = create(:question)
      post :vote_up, {user_id: question.user.id, id: question.id, question: question.as_json}
      expect(response.status).to eq(200)
    end
  end

  describe "POST vote_down" do
    it "has a 200 status code" do
      question = create(:question)
      post :vote_down, {user_id: question.user.id, id: question.id, question: question.as_json}
      expect(response.status).to eq(200)
    end
  end
end
