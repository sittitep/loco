class Message < ActiveRecord::Base
	validates :body, presence: true
	validates :user_id, presence: true
	belongs_to :user

	has_many :children, class_name: "Message", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Message"
end
