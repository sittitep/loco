require 'rails_helper'

params_user = {email: "example@example.com", password: "password"}
user = User.create! params_user
params_message = {body: 'test'}
message = user.messages.create! params_message

RSpec.describe Api::V1::MessagesController, type: :controller do
  describe "GET show" do
    it "has a 200 status code" do
      get :show, {id: message.id, user_id: user.id}
      expect(response.status).to eq(200)
    end
  end

	describe "POST create" do
    it "has a 200 status code" do
      post :create, {user_id: user.id, message: params_message}
      expect(response.status).to eq(200)
    end
  end

  describe "PUT update" do
    it "has a 200 status code" do
      put :update, {user_id: user.id, id: message.id, message: params_message}
      expect(response.status).to eq(200)
    end
  end
end
