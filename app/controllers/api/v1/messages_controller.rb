class Api::V1::MessagesController < ApplicationController
	before_action :set_user
	before_action :set_message, only: [:show, :update]

	def show
		if @message
			render json: @message, status: 200
		else
			render json: {}, status: 400
		end
	end

	def create
		message = @user.messages.new params_message
		if message.save
			render json: message, status: 200
		else
			render json: message.errors.as_json, status: 400
		end
	end

	def update
		if @message.update_attributes params_message
			render json: @message, status: 200
		else
			render json: @message.errors.as_json, status: 400
		end	
	end

	private
		def set_user
			@user = User.find_by_id params[:user_id]
		end

		def set_message
			@message = Message.find_by_id params[:id]
		end

		def params_message
			params.require(:message).permit(:header, :body)
		end
end
