class Api::V1::MessageSerializer < ActiveModel::Serializer
  attributes :id, :header, :body
  has_one :user
  has_many :children
end