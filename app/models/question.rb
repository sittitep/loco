class Question < Message
	validates :header, presence: true
	has_many :answers, foreign_key: "parent_id", class_name: "Answer"

	acts_as_votable
end
